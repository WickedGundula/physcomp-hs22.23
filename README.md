**😻Physical Computing LAB**

[[_TOC_]]

🐚 Basic Info

Start 16.45 ; Break 18.15-18.30 ; End 20.00

📔Description
Physical Computing is the creative design of interactive objects and spaces. The module provides a project based introduction to the design of interactive objects and spaces. 
In order to design interactive objects and spaces the students learn basic processes of combining soft- and hardware components. They learn how to use sensors and actuators as inputs and outputs in a concrete project using iterative processes in the development of a prototype with Arduino/Processing. They deepen their understanding of how the technological possibilities generate meaning. 

😎 Who is Sophie
Sophie Kellner is a designer and teacher and lives with her family in Basel. She studied textile engineering in BA in Albstadt (DE) and fashion and integrative design in MA in Basel (CH). She works exploratory, playful and experimental with conductive textile materials, interactive objects and spaces as well as physical computing. Sophie is particularly interested in questioning the aesthetics and functionality of electronics and the interplay of soft-hard-social skills through electronic textiles which can be explored collectivley and collaborativly. 

- Teaching craft and digital topics @ Haus der elektronischen Künste, Pädagogische Hochschule and the Vitra Design Museum, HGK 

- Currently finished her work in a research project on Ubiquitous Computing @ CML

- She is temporarily working as a freelance costume designer for productions at Theater Basel and Neues Theater Dornach and others

**Hompage sophiekellner.de**

✌️Motto
>  Staying with the trouble. - Donna Haraway

# 29.9. Introduction- A Way of Computing Physically - Lab Tea Room - ✋MAKE Tea Cups
🍪 Your turn!

- What is your name, discipline and interest
- Do you have any physical computing/coding experience? (it's fine if you don't)
- Why do you want to join this course? Do you have expectations?
- Do you have anything to share? Previous work/try-outs/photos/videos?

**A Way of Computing Physically**

🎨 Let's talk about this format

- Physical computing not just as tool, but as a way of living in the sense by integrating an attitude towards life in the making. Keyword: Coding otherwise = Coding political, social, anti-patriachahic, anti-neoliberal
Keyword: Techno-Feminsim

- I want to invite you to find out for yourselfs what it means for you to code with emphaty, to work as a community, to share and to take care and to reflect on actions and materials you use. 

- "Sensing Practices" 
-> sensing units for measuring our environment as independent working units that also process data and communicate
-> neglecting a purly human-centered perspective on environmental sensing, but implementing technologies and their sensing activities into this perspective
-> Sensing technology as organisms (see electronic textiles and organic materiality)

🎉 **Today is a foundation-day!** 
- Whats the LABs name? What is this Lab? Who is in this Lab? Where is the Lab? How about structure, organization, infrastructure?
- What is your role?
- How I see my role: curator who inspires you with ideas about "a way of computing physically", with a position towards physical Computing, with workflows, with projects...
I have 6 years of experience with Arduino projects, but I would not call myself an expert. There are no experts. What I want to share with you is a basic knowledge and the ability to "get what you want", to use open source online as a tool to get answers to you problems. I sugguest that we work collaborativley in an embracing, loving Lab atmosphere. 

**LAB Tea Room**

15min Tea Drinking Ceremony as symbolic and pragmatic way of discussing and forming the LAB

**Material Intervention**

What do we need to drink tea? ☕

# 6.10. Arduino Get to Know & ✋MAKE First Sketch
-> TODAY: Explore Electronics (first half) and Arduino Learning by Doing (Second Half)

**Explore Electronics**

Each group researches 1 topic + 5min share. 

!! The source is very detailled: try to filter the most important. Try to show an image/gif and explain briefly in a couple of sentences. Explain & Show what YOU needed to understand it!!

1. [Hydraulic Analogy](https://makeabilitylab.github.io/physcomp/electronics/electricity-basics.html#a-brief-overview)
2. [Current](https://makeabilitylab.github.io/physcomp/electronics/electricity-basics.html#what-is-current) (First 3 paragraphs) & [Voltage](https://makeabilitylab.github.io/physcomp/electronics/electricity-basics.html#what-is-voltage)
3. [Ohms Law](https://makeabilitylab.github.io/physcomp/electronics/ohms-law.html) paragraphs 1a, 2a, 3a
4. [Circuit](https://makeabilitylab.github.io/physcomp/electronics/schematics.html) headings 1 & 2 roughly
5. [Resistors](https://makeabilitylab.github.io/physcomp/electronics/resistors.html) headings 1,2,5

**Arduino Learning by Doing**

-> Groups of two 

-> Grab Arduino, USB cable, LED, resistor (calculate)

1. Download Arduino Software
2. Wire wrap LED & resistor, Plug in Arduino
3. LED Blink Sketch
4. Use Breadboard
5. Style your LED

Homework: Read [Chapter LED](https://makeabilitylab.github.io/physcomp/electronics/leds.html)

# 13.10. ✋ More Arduinio Learning by Doing: Piezo & Buttons

16.45-17.00 LAB Tea Room

17.00-17.15 Input on Silicon in technology

17.15-18.15 

- ✋Using Breadboards (https://makeabilitylab.github.io/physcomp/electronics/breadboards.html)
- Buttons (https://makeabilitylab.github.io/physcomp/arduino/buttons.html; Headings: 2a,b ; 3a ; 4c,d,e)
- Write LED+Button Code
- Hook Up
- Playing Around with LEDs & Switches

15min Break

18.30-18.45 What is a Piezo Material?

18.45-20.00 ✋Exploring Piezo Elements, Coding a Simple Piano, Experimenting with Switches out of different materials

Homework: Prepare research 

# 20.10. Circuit Design - Designing & Crafting Circuits

No LAB Tea Room today - but finishing your cups in the workshop!

17.00-18.00 Intro on Circuit Design- Examples & Material Show OFF - PROJECT TASK!

18.00-19.00 Start 

19.00-19.30 Project Present and Questions

19.30- ... Glaze your cup with tutor Marie-Louise at workshop!

TASK:

Design a circuit that communicates something (a message, a story, a joke, a poem, a warning). 
- State your message in 1-5 sentances (e.g. Flower is singing to a bee to attract her)
- Define the techniqual set-up behind it: code and electric components (e.g. LED blinking randomly; Arduino, red LED, 220Ohm resistor)
- Choose Materials: surface, conductive material for circuit...
- Draw a design and technical schematic on paper: define location of components according to design idea, define the circuit paths, sketch material add-ons. Circuit has to be right!
- Develop your idea with code and breadboards
- Gather materials and start crafting your physical circuit analog to your breadboard pretest

 

# 27.10. Circuit Design - Designing & Crafting Circuits

Orga: Who wants to come to Knitting workshop of fashion design for an extra hour on either Tuesday 8.11. or Thursday 10.11. in the afternoon to craft a stretch sensor? One person per team at least.

16.45 - 17.15 Lab Tea Room

💦💦💦 Let's (re)imagine. Imagine a swimming pool as a lab and draw it. Post it on slack 💦💦💦

17.15 - 20.00 Craft Project and finilize documentation on Slack and optionally a printed version.



# 03.11. Circuit Design - Designing & Crafting Circuits

**16.45-17.15 🍵Lab Tea Room🍵 @Civic**

Please bring your tea, your agenda, and pencils.

✍️ Draw a Physical Computing Lab that functions like a public swimming pool (or a tree, or a bee hive, or ...). Think about the zones, places, processes, structures, employees, visitors etc in a public swimming pool - how would a lab look like and function, if it had similar zones, places, processes, structures, employees, visitors...

See Fritz Kahn "Der Mensch als Industriepalast", 1926

We will discuss it next Lab Tea Room!

🧶 Date for Knitting (Tuesday 2h on 15.11. and/or 22.11.) -> Knitted Stretch Sensor Project on 24.11.

**17.30 Working on crafted circuit projects**

Mentoring in turns in Mentoring Corner

Finishing your project (physical circuit AND complete documentation with pictures, schematics) for our final session on 5.1.2023, where project I and project II will be presented. You can ask questions until then everytime we meet.


# 10.11. ✋MAKE Capacitive Circuits

16.45 -17h LAB Tea Room: Discuss Drawings

17-17.30h Intro in Capacitive Sensing & move to workshop

Design a capacitive Sensing Swatch with materials and techniques that shape a haptical divers landscape.
One circuit per person, Coding in teams of 2-3.

1. Circuit: capacitive sensor & led fade.

2. Sensor Crafting Techniques: Felting, Weaving, others

3. Coding

4. Documentation group photo & video


# 17.11. NO LAB - Junior Design Research Conference

# 22.11. 🧶 Knitting Stretch Sensor @Knitting Atelier Fashion Institute

15-17h: Introduction to knitting with a handmachine, knitting a stretch sensor with conductive yarn

# 24.11. ✋MAKE Stretch Sensors

16.45-17.15 Lab Tea Room: our thoughts on swimming pool Lab.

17.15-17.30: Knitt Sensor Swatch / Code / Task

17.30-20.00:

Remake your own stretch sensor swatch after the example & instruction. Design a visual backdrop for your swatch that integrates the stretch sensor as an interactive object. Glue the backdrop (hand drawing, digital drawing...) onto your kapa square and install the sensor on top.


# 01.12. Project

Two groups: Design a circuit that communicates something (a message, a story, a joke, a poem, a warning).

Define the story and the design of your project. 

Define coding team, crafting team, electronics team depending on skills and interests. 

The project should include an Arduino, compenents we worked with e.g. LEDs, piezos (as sensor or actuator) and can include new components like light sensor, vibration motor. Also it sould show a circuit that plays an aesthetic role in the story of the interactive object: consciously choose on conductive and non conductive materials. Like in Project I: Make a good circuit schematic and documentation photos, so someone else could redo your project.

Materials! Smooth and Seamless! Soft and Detailled (no techy style allowed).

# 08.12. Project

Making.

Design Project: Materials! Smooth and Seamless! Soft and Detailled (no techy style allowed).

# 15.12. Project

Debugging.

Design Project: Materials! Smooth and Seamless! Soft and Detailled (no techy style allowed).

# 05.01. Project Exhibit & Lab Party

# 📕Literature🔖learning resource
Sensing Practices https://research.gold.ac.uk/id/eprint/19512/1/GabrysPritchard_SensingPractices_Posthuman_FINAL.pdf (homeprep for 29.9.)


